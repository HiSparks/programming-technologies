---
title: "Programming technologies"
show_comments: false
---

Course: **Programming technologies**

Lecturer: *PhD, Associate Professor. Eugene Babeshko*

Contacts:
- Email: e.babeshko@csn.khai.edu
